using System;
using System.Collections.Generic;
using System.Linq;
using Autofac.Extras.Moq;
using ConsoleEShop_Middle;
using Moq;
using Xunit;

namespace ConsoleEShop_Middle.Tests
{
    public class ConsoleEShopTest
    {
        #region Init
        private readonly List<Product> _products;
        private readonly List<Order> _orders;
        private readonly List<User> _users;
        private readonly DBForTesting _db;

        public ConsoleEShopTest()
        {
            _products = new List<Product>()
            {
                new Product("Apple", 1.2m),
                new Product("Banana", 2),
                new Product("Orange", 1.7m),
                new Product("Grapes", 3)
            };

            _orders = new List<Order>()
            {
                new Order
                (
                    userId: 0,
                    orderedProducts: new Dictionary<Product, int>()
                    {
                        [_products[0]] = 1,
                        [_products[1]] = 5
                    },
                    DateTime.Now
                )
            };

            _users = new List<User>()
            {
                new User("Ivan", "Ivan@gmail.com", "password"),
                new User("admin", "admin@epam.com", "admin", true)
            };

            _db = new DBForTesting(_products, _orders, _users);
        }

        #endregion

        #region DataBase

        [Fact]
        public void GetAllProducts_CallMethod_ReturnListOfProducts()
        {
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IRepository>()
                    .Setup(x => x.GetAllProducts())
                    .Returns(_products);

                var cls = mock.Create<IRepository>();
                var expected = _products;

                var actual = cls.GetAllProducts();

                Assert.NotNull(actual);
                Assert.Equal(expected, actual);
            }         
        }

        [Fact]
        public void GetAllUsers_CallMethod_ReturnListOfUsers()
        {
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IRepository>()
                    .Setup(x => x.GetAllUsers())
                    .Returns(_users);

                var cls = mock.Create<IRepository>();
                var expected = _users;

                var actual = cls.GetAllUsers();

                Assert.NotNull(actual);
                Assert.Equal(expected, actual);
            }
        }

        [Fact]
        public void GetAllOrders_CallMethod_ReturnListOfOrders()
        {
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IRepository>()
                    .Setup(x => x.GetAllOrders())
                    .Returns(_orders);

                var cls = mock.Create<IRepository>();
                var expected = _orders;

                var actual = cls.GetAllOrders();

                Assert.NotNull(actual);
                Assert.Equal(expected, actual);
            }
        }

        [Fact]
        public void AddOrder_InputNewOrder_IncreaseCountOfOrdersList()
        {
            using (var mock = AutoMock.GetLoose())
            {
                var orderToAdd = new Order(3, new Dictionary<Product, int>(), DateTime.Now);
                
                mock.Mock<IRepository>()
                    .Setup(x => x.AddOrder(orderToAdd));

                var cls = mock.Create<IRepository>();
                cls.AddOrder(orderToAdd);

                mock.Mock<IRepository>()
                    .Verify(x => x.AddOrder(orderToAdd), Times.Exactly(1));
            }
        }

        [Fact]
        public void RemoveOrder_InputOrderToRemove_DecreaseCountOfOrdersList()
        {
            using (var mock = AutoMock.GetLoose())
            {
                var orderToDelete = _orders[0];

                mock.Mock<IRepository>()
                    .Setup(x => x.DeleteOrder(orderToDelete));

                var cls = mock.Create<IRepository>();
                cls.DeleteOrder(orderToDelete);

                mock.Mock<IRepository>()
                    .Verify(x => x.DeleteOrder(orderToDelete), Times.Exactly(1));
            }
        }

        [Fact]
        public void GetOrderById_InputIdOfOrder_ReturnOrder()
        {
            using (var mock = AutoMock.GetLoose())
            {
                int orderId = _orders.First().Id;

                mock.Mock<IRepository>()
                    .Setup(x => x.GetOrderById(orderId))
                    .Returns(_orders.FirstOrDefault(o => o.Id == orderId));

                var cls = mock.Create<IRepository>();
                var expected = _orders.FirstOrDefault(o => o.Id == orderId);

                var actual = cls.GetOrderById(orderId);

                Assert.NotNull(actual);
                Assert.Equal(expected, actual);
            }
        }

        [Fact]
        public void GetOrdersForUser_InputUserId_ReturnListOfOrders()
        {
            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<IRepository>()
                    .Setup(x => x.GetOrdersForUser(0))
                    .Returns(_orders.Where(o => o.UserId == 0).ToList());

                var cls = mock.Create<IRepository>();
                var expected = _orders.Where(o => o.UserId == 0).ToList();

                var actual = cls.GetOrdersForUser(0);

                Assert.NotNull(actual);
                Assert.Equal(expected, actual);
            }
        }

        [Fact]
        public void GetProductByName_InputStringName_ReturnProduct()
        {
            using (var mock = AutoMock.GetLoose())
            {
                string name = "Apple";

                mock.Mock<IRepository>()
                    .Setup(x => x.GetProductByName(name))
                    .Returns(_products.FirstOrDefault(o => o.Name == name));

                var cls = mock.Create<IRepository>();
                var expected = _products.FirstOrDefault(o => o.Name == name);

                var actual = cls.GetProductByName(name);

                Assert.NotNull(actual);
                Assert.Equal(expected, actual);
            }
        }

        [Fact]
        public void GetProductById_InputProductId_ReturnProduct()
        {
            using (var mock = AutoMock.GetLoose())
            {
                int productId = 1;

                mock.Mock<IRepository>()
                    .Setup(x => x.GetProductById(productId))
                    .Returns(_products.FirstOrDefault(o => o.Id == productId));

                var cls = mock.Create<IRepository>();
                var expected = _products.FirstOrDefault(o => o.Id == productId);

                var actual = cls.GetProductById(productId);

                Assert.NotNull(actual);
                Assert.Equal(expected, actual);
            }
        }

        [Fact]
        public void AddProduct_InputProduct_IncreaseCountOfListProducts()
        {
            using (var mock = AutoMock.GetLoose())
            {
                var productToAdd = new Product("", 0);

                mock.Mock<IRepository>()
                    .Setup(x => x.AddProduct(productToAdd));

                var cls = mock.Create<IRepository>();
                cls.AddProduct(productToAdd);

                mock.Mock<IRepository>()
                    .Verify(x => x.AddProduct(productToAdd), Times.Exactly(1));
            }
        }

        [Fact]
        public void AddUser_InputUser_IncreaseCountOfUserList()
        {
            using (var mock = AutoMock.GetLoose())
            {
                var userToAdd = new User();

                mock.Mock<IRepository>()
                    .Setup(x => x.AddUser(userToAdd));

                var cls = mock.Create<IRepository>();
                cls.AddUser(userToAdd);

                mock.Mock<IRepository>()
                    .Verify(x => x.AddUser(userToAdd), Times.Exactly(1));
            }
        }

        [Fact]
        public void GetUserByName_InputUserName_ReturnUser()
        {
            using (var mock = AutoMock.GetLoose())
            {
                string userName = "Ivan";

                mock.Mock<IRepository>()
                    .Setup(x => x.GetUserByName(userName))
                    .Returns(_users.FirstOrDefault(o => o.Name == userName));

                var cls = mock.Create<IRepository>();
                var expected = _users.FirstOrDefault(o => o.Name == userName);

                var actual = cls.GetUserByName(userName);

                Assert.NotNull(actual);
                Assert.Equal(expected, actual);
            }
        }

        [Fact]
        public void GetUserById_InputUserId_ReturnUser()
        {
            using (var mock = AutoMock.GetLoose())
            {
                int userId = _users.First().Id;

                mock.Mock<IRepository>()
                    .Setup(x => x.GetUserById(userId))
                    .Returns(_users.FirstOrDefault(o => o.Id == userId));

                var cls = mock.Create<IRepository>();
                var expected = _users.FirstOrDefault(o => o.Id == userId);

                var actual = cls.GetUserById(userId);

                Assert.NotNull(actual);
                Assert.Equal(expected, actual);
            }
        }

        #endregion

        #region GuestRole
        #endregion
    }
}
