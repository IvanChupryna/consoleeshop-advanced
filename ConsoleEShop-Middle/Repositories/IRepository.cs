﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public interface IRepository
    {
        public List<Product> GetAllProducts();
        public Product GetProductByName(string name);
        public Product GetProductById(int productId);
        public void AddProduct(Product product);
        public List<Order> GetAllOrders();
        public List<Order> GetOrdersForUser(int userId);
        public Order GetOrderById(int orderId);
        public void AddOrder(Order order);
        public void DeleteOrder(Order order);
        public void AddUser(User user);
        public List<User> GetAllUsers();
        public User GetUserByName(string username);
        public User GetUserById(int userId);
    }
}
