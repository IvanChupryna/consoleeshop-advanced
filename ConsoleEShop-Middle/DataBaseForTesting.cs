﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShop_Middle
{
    public class DataBaseForTesting : IRepository
    {
        List<Product> _products = new List<Product>()
        {
            new Product("Apple", 1.2m),
            new Product("Banana", 2),
            new Product("Orange", 1.7m),
            new Product("Grapes", 3)
        };

        List<Order> _orders = new List<Order>()
        {
            new Order
            (
                userId: 0,
                orderedProducts: new Dictionary<Product, int>()
                {
                    [new Product("Apple", 1.2m)] = 1,
                    [new Product("Banana", 2)] = 5
                },
                DateTime.Now
            )
        };

        List<User> _users = new List<User>()
        {
            new User("lol", "lol", "lol"),
            new User("admin", "admin", "admin", true)
        };
        public void AddOrder(Order order)
        {
            _orders.Add(order);
        }

        public void DeleteOrder(Order order)
        {
            _orders.Remove(order);
        }

        public List<Order> GetAllOrders()
        {
            return _orders;
        }
        public List<Product> GetAllProducts()
        {
            return _products;
        }

        public Order GetOrderById(int orderId)
        {
            return _orders.Find(order => order.Id == orderId);
        }

        public List<Order> GetOrdersForUser(int userId)
        {
            return _orders.FindAll(order => order.UserId == userId);
        }

        public Product GetProductByName(string name)
        {
            return _products.Find(product => product.Name == name);
        }

        public void AddUser(User user)
        {
            _users.Add(user);
        }
        public List<User> GetAllUsers()
        {
            return _users;
        }
        public User GetUserByName(string username)
        {
            return _users.FirstOrDefault(user => user.Name == username);
        }
        public User GetUserById(int userId)
        {
            return _users.FirstOrDefault(user => user.Id == userId);
        }

        public void AddProduct(Product product)
        {
            _products.Add(product);
        }

        public Product GetProductById(int productId)
        {
            return _products.FirstOrDefault(product => product.Id == productId);
        }
    }
}
