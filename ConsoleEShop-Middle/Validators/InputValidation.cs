﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    class InputValidation
    {
        static public string CheckInput<T>(string input, out T convertedOutput)
        {
            try
            {
                convertedOutput = (T)Convert.ChangeType(input, typeof(T));
            }
            catch(Exception)
            {
                convertedOutput = default(T);
                return "Input data has invalid format";
            }
            
            return String.Empty;
        }
    }
}
