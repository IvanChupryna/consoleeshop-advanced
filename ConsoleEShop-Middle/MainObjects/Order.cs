﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ConsoleEShop_Middle
{
    public class Order
    {
        private static int nextId = 0;
        public int Id { get; }
        public int UserId { get; set; }
        public Dictionary<Product, int> Products { get; set; }
        public DateTime Date { get; set; }
        public Status Status { get; set; } = Status.New;
        public decimal TotalPrice
        {
            get => (from product in Products
                   select product.Key.Price * product.Value).Sum();
        }


        public Order(int userId, Dictionary<Product, int> orderedProducts, DateTime orderDate)
        {
            Id = nextId++;
            UserId = userId;
            Products = orderedProducts;
            Date = orderDate;
        }

        public override string ToString()
        {
            string res = $"Order Id: {Id}, UserId: {UserId}, Date: {Date}, Status: {Status}\nProducts:\n";
            string listOfProducts = "";
            foreach(var product in Products)
            {
                listOfProducts = listOfProducts + $"\t{product.Key}:, Quantity: {product.Value} units\n";
            }

            return res + listOfProducts + $"Total Price: {TotalPrice}$";

        }
    }
}
