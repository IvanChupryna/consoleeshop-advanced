﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ConsoleEShop_Middle
{
    public class UserRole : BaseRole, IRole
    {
        private User CurrentUser { get; }

        protected Dictionary<Product, int> _cart;

        public delegate void LogOutHandler();
        public event LogOutHandler LogOutEvent;

        public UserRole(User user, IRepository repository) : base(repository)
        {
            CurrentUser = user;
            _repository = repository;
            _cart = new Dictionary<Product, int>();
        }
        
        public string AddProductToCart()
        {
            Console.WriteLine(GetAvailableProducts());
            Console.Write("Pass the id of product: ");
            string res = InputValidation.CheckInput<int>(Console.ReadLine().Trim(), out int productId);
            if (res != String.Empty) 
            {
                return res;
            }

            Product selectedProduct = _repository.GetProductById(productId);
            if(selectedProduct is null)
            {
                return "There's no product with such name";
            }
            if (_cart.ContainsKey(selectedProduct))
            {
                return "You already added this product to your Cart";
            }

            Console.Write("Pass the number of units of product: ");

            res = InputValidation.CheckInput<int>(Console.ReadLine().Trim(), out int productCount);
            if (res != String.Empty)
            {
                return res;
            }

            _cart.Add(selectedProduct, productCount);
            return $"Product with Id {productId} is successfully added to the cart";
        }

        public string RemoveProductFromCart()
        {
            Console.WriteLine(ShowProductsInCart());

            Console.Write("Pass the Id of product: ");
            string res = InputValidation.CheckInput<int>(Console.ReadLine().Trim(), out int productId);
            if (res != String.Empty)
            {
                return res;
            }

            Product selectedProduct = _repository.GetProductById(productId);
            if (!_cart.ContainsKey(selectedProduct))
            {
                return "There's no product with such name in your Cart";
            }

            _cart.Remove(selectedProduct);
            return $"Product with Id {productId} has been removed from your cart";
        }

        public string ShowProductsInCart()
        {
            if(_cart.Count == 0)
            {
                return "Cart is empty";
            }

            string res = String.Empty;
            foreach(KeyValuePair<Product, int> product in _cart)
            {
                res += product.Key + $" Quantity: {product.Value}" + "\n";
            }
            return res;
        }
        public string EditProductQuantityInCart()
        {
            Console.WriteLine(ShowProductsInCart());
            Console.Write("Enter Id of a product: ");

            string res = InputValidation.CheckInput<int>(Console.ReadLine().Trim(), out int productId);
            if (res != String.Empty)
            {
                return res;
            }

            Product selectedProduct = _repository.GetProductById(productId);
            if (!_cart.ContainsKey(selectedProduct))
            {
                return "There's no such Product in your Cart";
            }

            Console.Write("Pass new quantity: ");

            res = InputValidation.CheckInput<int>(Console.ReadLine().Trim(), out int newQuantity);
            if (res != String.Empty)
            {
                return res;
            }

            _cart[selectedProduct] = newQuantity;
            return $"Quantity of a Product with Id {selectedProduct.Id} is changed to {newQuantity}";   
        }
        public string ClearCart()
        {
            _cart.Clear();
            return "Cart is successfully cleared";
        }

        public string PlaceOrder()
        {
            if(_cart.Count == 0)
            {
                return "There's no product in your Cart";
            }

            Dictionary<Product, int> cartCopy = new Dictionary<Product, int>();
            foreach(var product in _cart)
            {
                cartCopy.Add(product.Key, product.Value);
            }

            Order currentOrder = new Order(CurrentUser.Id, cartCopy, DateTime.Now);
            _repository.AddOrder(currentOrder);
            ClearCart();

            return $"Your order with Id {currentOrder.Id} is successfully placed";
        }

        public string CancelOrder()
        {
            Console.WriteLine(GetOrdersHistory());

            Console.Write("Write Order Id: ");
            string res = InputValidation.CheckInput<int>(Console.ReadLine(), out int orderId);
            if (res != String.Empty)
            {
                return res;
            }

            Order selectedOrder = _repository.GetOrderById(orderId);
            if (selectedOrder is null)
            {
                return "There's no order with such Id";
            }

            selectedOrder.Status = Status.CanceledByUser;
            _repository.DeleteOrder(selectedOrder);

            return $"Order with Id {orderId} has been successfully canceled";
        }

        public string GetOrdersHistory()
        {
            List<Order> history = _repository.GetOrdersForUser(CurrentUser.Id);
            if(history.Count == 0)
            {
                return "You haven't placed any orders yet";
            }

            string res = String.Empty;
            foreach(Order order in history)
            {
                res += order.ToString() + "\n";
            }
            return res;
        }

        public string SetRecievedStatusToOrder()
        {
            Console.WriteLine(GetOrdersHistory());

            Console.Write("Write Order Id: ");
            string res = InputValidation.CheckInput<int>(Console.ReadLine(), out int orderId);
            if (res != String.Empty)
            {
                return res;
            }

            Order currentOrder = _repository.GetOrderById(orderId);
            if (currentOrder is null)
            {
                return "There's no order with such Id";
            }

            currentOrder.Status = Status.Received;
            return "Status is successfully changed";
        }

        public string  ChangeName()
        {
            Console.Write("Write new username");
            string newName = Console.ReadLine().Trim();
            if(newName == CurrentUser.Name)
            {
                return "No changes were made";
            }

            CurrentUser.Name = newName;
            return $"Username changed to {newName}";
        }

        public string ChangeEmail()
        {
            Console.Write("Write new email");
            string newEmail = Console.ReadLine().Trim();
            if (!new Regex(@"\w+\@\w+\.\w+").IsMatch(newEmail))
            {
                return "Email in wrong format";
            }
            if (newEmail == CurrentUser.Email)
            {
                return "No changes were made";
            }

            CurrentUser.Name = newEmail ;
            return $"Email changed to {newEmail}";
        }

        public string LogOut()
        {
            LogOutEvent?.Invoke();
            return "Logging out...";
        }

        private protected string ExecuteAction(string userChoice)
        {
            Console.Clear();
            var actionDict = GetActionDict();

            try
            {
                return actionDict[userChoice]();
            }
            catch
            {
                return "Wrong Input";
            }
        }

        protected virtual void PrintActions()
        {
            Console.Clear();
            Console.WriteLine("1 - Get Available Products");
            Console.WriteLine("2 - Search Product");
            Console.WriteLine("3 - Add Product To Cart");
            Console.WriteLine("4 - Remove Product From Cart");
            Console.WriteLine("5 - Edit Product Quantity In Cart");
            Console.WriteLine("6 - Show Products In Cart");
            Console.WriteLine("7 - Clear Cart");
            Console.WriteLine("8 - Place Order");
            Console.WriteLine("9 - Get Orders History");
            Console.WriteLine("10 - Cancel Order");
            Console.WriteLine("11 - Set Recieved Status To Order");
            Console.WriteLine("12 - Change Name");
            Console.WriteLine("13 - Change Email");
            Console.WriteLine("14 - Log Out");
            Console.WriteLine("15 - Exit");
        }

        public string ChooseAction()
        {
            PrintActions();

            Console.Write("What Do you want to do: ");
            return ExecuteAction(Console.ReadLine().Trim());
        }

        protected virtual Dictionary<string, Func<string>> GetActionDict()
        {
            return new Dictionary<string, Func<string>>()
            {
                ["1"] = GetAvailableProducts,
                ["2"] = SearchProduct,
                ["3"] = AddProductToCart,
                ["4"] = RemoveProductFromCart,
                ["5"] = EditProductQuantityInCart,
                ["6"] = ShowProductsInCart,
                ["7"] = ClearCart,
                ["8"] = PlaceOrder,
                ["9"] = GetOrdersHistory,
                ["10"] = CancelOrder,
                ["11"] = SetRecievedStatusToOrder,
                ["12"] = ChangeName,
                ["13"] = ChangeEmail,
                ["14"] = LogOut,
                ["15"] = Exit
            };
        }
    }
}
