﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleEShop_Middle
{
    public class AdministratorRole : UserRole
    {
        public AdministratorRole(User user, IRepository repository) : base(user, repository) { }

        public string ShowAllUsersInfo()
        {
            string res = String.Empty;
            foreach(User user in _repository.GetAllUsers())
            {
                res += user.ToString() + "\n";
            }
            return res;
        }

        public string ChangeUserName()
        {
            Console.WriteLine(ShowAllUsersInfo());
            Console.Write("Input id of a User name of which you want to change: ");

            string res = InputValidation.CheckInput<int>(Console.ReadLine().Trim(), out int userId);
            if (res != String.Empty)
            {
                return res;
            }

            User user = _repository.GetUserById(userId);
            if (user is null)
            {
                return "There's no user with such Id";
            }
            UserRole temp = new UserRole(user, _repository);
            return temp.ChangeName();
        }

        public string ChangeUserEmail()
        {
            Console.WriteLine(ShowAllUsersInfo());
            Console.Write("Input id of a User email of which you want to change: ");

            string res = InputValidation.CheckInput<int>(Console.ReadLine().Trim(), out int userId);
            if (res != String.Empty)
            {
                return res;
            }

            User user = _repository.GetUserById(userId);
            if (user is null)
            {
                return "There's no user with such Id";
            }
            UserRole temp = new UserRole(user, _repository);
            return temp.ChangeEmail();
        }

        public string AddProductToCatalog()
        {
            Console.Write("Product Name: ");
            string name = Console.ReadLine().Trim();
            if (!(_repository.GetProductByName(name) is null))
            {
                return "There's already Product with such name";
            }

            Console.Write("Product Price: ");
            string res = InputValidation.CheckInput<decimal>(Console.ReadLine().Trim(), out decimal price);
            if (res != String.Empty)
            {
                return res;
            }

            _repository.AddProduct(new Product(name, price));
            return $"Product {name} successfully added to Catalog";
        }

        public string ChangeProductInfo()
        {
            GetAvailableProducts();
            Console.Write("Write Product Id: ");
            string res = InputValidation.CheckInput<int>(Console.ReadLine().Trim(), out int productId);
            if (res != String.Empty)
            {
                return res;
            }

            Product productToEdit = _repository.GetProductById(productId);
            if(productToEdit is null)
            {
                return "There's no product with such Id";
            }

            Console.Write("New Name: ");
            string newName = Console.ReadLine().Trim();

            Console.Write("New Price: ");
            res = InputValidation.CheckInput<decimal>(Console.ReadLine().Trim(), out decimal newPrice);
            if (res != String.Empty)
            {
                return res;
            }

            res = String.Empty;
            if (!(newName is null))
            {
                res += $"Product Name changed from {productToEdit.Name} to {newName}\n";
                productToEdit.Name = newName;
            }
            if(newPrice != 0) 
            {
                res += $"Product Price changed from {productToEdit.Price} to {newPrice}\n";
                productToEdit.Price = newPrice;
            }
            return res;
        }

        private void ShowAllOrders()
        {
            List<Order> orders = _repository.GetAllOrders();
            foreach(var order in orders)
            {
                Console.WriteLine(order + "\n");
            }
        }
        public string ChangeOrderStatus()
        {
            ShowAllOrders();

            Console.Write("Write Order Id: ");
            string res = InputValidation.CheckInput<int>(Console.ReadLine().Trim(), out int orderId);
            if (res != String.Empty)
            {
                return res;
            }

            Order orderToEdit = _repository.GetOrderById(orderId);
            if(orderToEdit is null)
            {
                return "There's no order with such Id";
            }

            Console.Write("Available statuses:");
            foreach(var status in Enum.GetValues(typeof(Status)))
            {
                Console.WriteLine($"\t{status} - {(int)status}");
            }

            Console.Write("Choose Status: ");
            res = InputValidation.CheckInput<int>(Console.ReadLine().Trim(), out int statusNum);
            if (res != String.Empty)
            {
                return res;
            }

            if (statusNum == 0)
            {
                return "There's no status with such Number";
            }
            return $"Status of Order with Id {orderId} has changed to {orderToEdit.Status}";
        }

        protected override void PrintActions()
        {
            Console.Clear();
            Console.WriteLine("1 - Get Available Products");
            Console.WriteLine("2 - Search Product");
            Console.WriteLine("3 - Add Product To Cart");
            Console.WriteLine("4 - Remove Product From Cart");
            Console.WriteLine("5 - Edit Product Quantity In Cart");
            Console.WriteLine("6 - Show Products In Cart");
            Console.WriteLine("7 - Clear Cart");
            Console.WriteLine("8 - Place Order");
            Console.WriteLine("9 - Get Orders History");
            Console.WriteLine("10 - Cancel Order");
            Console.WriteLine("11 - Show All Users Info");
            Console.WriteLine("12 - Change User Name");
            Console.WriteLine("13 - Change User Email");
            Console.WriteLine("14 - Add Product To Catalog");
            Console.WriteLine("15 - Change Product Info");
            Console.WriteLine("16 - Change Order Status");
            Console.WriteLine("17 - Log Out");
            Console.WriteLine("18 - Exit");
        }
        protected override  Dictionary<string, Func<string>> GetActionDict()
        {
            return new Dictionary<string, Func<string>>()
            {
                ["1"] = GetAvailableProducts,
                ["2"] = SearchProduct,
                ["3"] = AddProductToCart,
                ["4"] = RemoveProductFromCart,
                ["5"] = EditProductQuantityInCart,
                ["6"] = ShowProductsInCart,
                ["7"] = ClearCart,
                ["8"] = PlaceOrder,
                ["9"] = GetOrdersHistory,
                ["10"] = CancelOrder,
                ["11"] = ShowAllUsersInfo,
                ["12"] = ChangeUserName,
                ["13"] = ChangeUserEmail,
                ["14"] = AddProductToCatalog,
                ["15"] = ChangeProductInfo,
                ["16"] = ChangeOrderStatus,
                ["17"] = LogOut,
                ["18"] = Exit
            };
        }
    }
}
