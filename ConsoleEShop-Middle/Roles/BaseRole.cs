﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public abstract class BaseRole
    {
        protected IRepository _repository;
        public bool IsActive { get; set; } = true;

        public BaseRole(IRepository repository)
        {
            _repository = repository;
        }

        public string GetAvailableProducts()
        {
            List<Product> availableProducts = _repository.GetAllProducts();

            string res = String.Empty;
            foreach (Product product in availableProducts)
            {
                res += product.ToString() + "\n";
            }
            return res;
        }

        public string SearchProduct()
        {
            Console.Write("Enter name of a product you want to find: ");
            string productName = Console.ReadLine().Trim();
            Product searchedProduct = _repository.GetProductByName(productName);

            if (searchedProduct is null)
            {
                return "Not Found";
            }
            else
            {
                return searchedProduct.ToString();
            }
        }
        public string Exit()
        {
            IsActive = false;
           return "Exiting Shop...";
        }
    }
}
