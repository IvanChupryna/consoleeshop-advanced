﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ConsoleEShop_Middle
{
    public class GuestRole : BaseRole, IRole
    {
        public GuestRole(IRepository repository) : base(repository) { }

        public delegate void AuthorizeHandler(User user);
        public event AuthorizeHandler AuthorizeEvent;

        public string RegisterNewUser()
        {
            Console.Write("Username: ");
            string userName = Console.ReadLine().Trim();
            if(!(_repository.GetUserByName(userName) is null))
            {
                return "User with such name already exists";
            }

            Console.Write("Email: ");
            string email = Console.ReadLine().Trim();
            if (!new Regex(@"\w+\@\w+\.\w+").IsMatch(email))
            {
                return "Email in wrong format";
            }
            if(_repository.GetAllUsers().Select(user => user.Email).Contains(email))
            {
                return "This email is already in use";
            }

            Console.Write("Password: ");
            string password = Console.ReadLine().Trim();

            User user = new User(userName, email, password);
            _repository.AddUser(user);
            return "Registration went successfull";
        }

        public string Authorize()
        {
            Console.Write("Username: ");
            string userName = Console.ReadLine().Trim();

            Console.Write("Password: ");
            string password = Console.ReadLine();

            User currentUser = _repository.GetUserByName(userName);

            if (currentUser is null)
            {
                return "There's no user with such UserName";
            }
            if(password != currentUser.Password)
            {
                return "Wrong Password";
            }

            AuthorizeEvent?.Invoke(currentUser);
            return "Authorized successfully";
        }

        protected string ExecuteAction(string userChoice)
        {
            Console.Clear();
            var actionDict = GetActionDict();

            try
            {
                return actionDict[userChoice]();
            }
            catch
            {
                return "Wrong Input";
            }
        }

        public string ChooseAction()
        {
            PrintActions();

            Console.Write("What Do you want to do: ");
            return ExecuteAction(Console.ReadLine().Trim());
        }

        private void PrintActions()
        {
            Console.Clear();
            Console.WriteLine("1 - Get Available Products");
            Console.WriteLine("2 - Search Product");
            Console.WriteLine("3 - Register");
            Console.WriteLine("4 - Authorize");
            Console.WriteLine("5 - Exit Shop");
        }

        private Dictionary<string, Func<string>> GetActionDict()
        {
            return new Dictionary<string, Func<string>>()
            {
                ["1"] = GetAvailableProducts,
                ["2"] = SearchProduct,
                ["3"] = RegisterNewUser,
                ["4"] = Authorize,
                ["5"] = Exit
            };
        }
    }
}
