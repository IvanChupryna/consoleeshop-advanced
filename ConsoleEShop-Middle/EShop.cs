﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    class EShop
    {

        private IRole role;
        private IRepository _db;
        public EShop(IRepository db)
        {
            _db = db;
        }

        public void OpenShop()
        {
            role = new GuestRole(_db);
            (role as GuestRole).AuthorizeEvent += Authorize;

            while ((role as BaseRole).IsActive)
            {
                Console.WriteLine(role.ChooseAction());
                Console.Write("Press Any Key To Continue...");
                Console.ReadKey();
            }
        }

        public void Authorize(User user)
        {
            if (user.IsAdmin)
            {
                role = new AdministratorRole(user, _db);
            }
            else
            {
                role = new UserRole(user, _db);
            }
            (role as UserRole).LogOutEvent += LogOut;
        }

        public void LogOut()
        {
            role = new GuestRole(_db);
            (role as GuestRole).AuthorizeEvent += Authorize;
        }
    }
}
